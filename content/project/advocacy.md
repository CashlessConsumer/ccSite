---
title: "Cashless Consumer Advocacy"
description: "All advocacy initiatives towards Cashless Consumer"
date: 2018-09-24T02:16:58-08:00
draft: false
categories: ['advocacy']
tags: ['advocacy']
---

---

Advocacy involves reaching out to more people through participation in community meetups, talks at seminars, conferences, workshops. We also hold CashlessConsumer [online meetups / study circles](https://hasgeek.com/cashlessconsumer) periodically. Please [Contact](http://cashlessconsumer.in/about/contact) for speaking requests in your events.

# CashlessConsumer meetups

|SNo|Topic - Event Page|Speakers|Video - Slides|Links|
|:---:|:---:|:---:|:---:|:---:|
| 1 | [All things IFSC codes](https://hasgeek.com/cashlessconsumer/payment-fintech-deepdives/) | Abhay Rana | [Video](https://youtube.com/watch?v=dQS1oakAcdc) - [Slides](https://captnemo.in/talks/ifsc/) | [Project Page](https://github.com/razorpay/ifsc) |
| 2 | [Anti Money Laundering (AML) in Remittances, Dataflows and Privacy](https://hasgeek.com/cashlessconsumer/aml-remittances/) | Kanchan Kumar, REMITR | [Video](https://youtube.com/watch?v=5IjafusUMXQ) - [Slides](https://www.slideshare.net/secret/i62r90PZY6bo7f) | [101 on AML](https://medium.com/cashlessconsumer/all-about-anti-money-laundering-aml-in-remittances-dataflows-and-privacy-fc0dd19c7191) - [Summary](https://medium.com/cashlessconsumer/anti-money-laundering-aml-in-remittances-dataflows-and-privacy-8af163790b84) |
| 3 | [Study Circle - Bharat Bill Payment System](https://hasgeek.com/cashlessconsumer/study-circle-bharat-bill-payment-system/) | [Akshay Bhalotia](https://twitter.com/akshay_bhalotia), Srikanth L | [Video](https://youtube.com/watch?v=JCWhp2xTFsA) - [Slides](https://docs.google.com/presentation/d/1zUoTKYJlXGoEuXgOLMsMJnc5Pi1kmDpyS_u9-ebOMNI/edit?usp=sharing) | [Summary](https://medium.com/cashlessconsumer/study-circle-bharat-bill-payment-system-85bd01a061b8) |
| 4 | [ATMs and Cashnomics](https://hasgeek.com/cashlessconsumer/atm-tech-and-cashonomics/) | Srikanth L | [Video](https://youtube.com/watch?v=ZNdGid5xBLo) - [Slides](https://docs.google.com/presentation/d/1xxuFZg7CV2EAH_cZqmOVI0D-PfoCc30F_KoDVvFViRY/edit?usp=sharing)| [Committee Report](https://pdfhost.io/v/LKvzuRX.G_CashlessConsumer_ATM_Interchange_Committee_Report.pdf)
| 5 | [A New Era for Credit Scoring - Paper Presentation](https://hasgeek.com/cashlessconsumer/a-new-era-for-credit-scoring-paper-presentation/) | Tarunima Prabhakar, Beni Chugh, Praneeth Bodduluri | [Video](https://youtube.com/watch?v=71OMERBTclA&feature=emb_logo) | [Paper](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3579695) |
| 6 | [Security Analysis of Unified Payments Interface and Payment Apps in India - Paper presentation](https://hasgeek.com/cashlessconsumer/security-analysis-of-unified-payments-interface-and-payment-apps-in-india-paper-presentation/) | Renuka Kumar | [Video](https://youtube.com/watch?v=J1tRCIZ7DjQ) | [Paper](https://www.usenix.org/system/files/sec20-kumar.pdf) |
| 7 | [Study Circle on Account Aggregators](https://hasgeek.com/cashlessconsumer/study-circle-on-account-aggregators/) |  Rohit Kilam, Aditya Birla Finance & Vinay Sathyanarayana, Perifos | [Video](https://youtube.com/watch?v=BtaOxKFRlUU) | NA |
| 8 | [KillerApps - Detecting Predatory fintech apps](https://hasgeek.com/cashlessconsumer/killerloanapps-detecting-fake-fintech-apps/) | Suman Kar, Banbreach | [Video](https://youtube.com/watch?v=OV7PCvfsF-A) - [Slides](https://1drv.ms/p/s!Au4kkP8CklsegbU_6MZHhyTbb_uy7w?e=SYMtob) | [Project Page](https://www.cashlessconsumer.in/post/killerloanapps/) |


# Talks

|Type|Topic|Event|Date|Video / Slides|
|:---:|:---:|:---:|:---:|:---:|
| Community Meetup | Cashless Consumer - Why?  | [Lamakaan](http://web.archive.org/web/20180924115451/http://admin.lamakaan.com/events/3680) | 24 Dec 2016 |  [Slides](https://drive.google.com/open?id=1VWtXcDgyLOgDR-f2b32zhJGuMyG6RRgzoRtQk8mlRsI)|
| Talk | UPI & Beyond - How communities can help consumers in cashless world | [50p 2017](https://50p.in/2017/) | 24 Jan 2017 | [Video](https://www.youtube.com/watch?v=ZeolwOp9sk8) / [Slides](https://docs.google.com/presentation/d/12mQz9OlP5WrGdv4fTxQkY1ikXUgClsrh60ulSRHwy6g/edit?usp=sharing)|
| Workshop | CIS Digital Payments Master class 2017 || 26 Jan 2017 | |
| Panel | Demonetisation and Digital Infrastructure | [4ccon](https://fsftn.gitlab.io/4ccon/) | 27 Jan 2017 | [Video](https://www.youtube.com/watch?v=U60Pj0kyiHQ)|
| Talk | Know your App - App Economy Accountability | [Devcon Hyderabad](http://devconhyd.com/) | 14 Oct 2017 | [Slides](https://docs.google.com/presentation/d/1LdnzSzPsKQFJqubdpvS1v5mCNTZjh3TSQLQZjpMptuo)|
| Talk | State of UPI - Observations from reluctant user | [50p 2018](https://50p.in/2018/) | 8 Feb 2018 | [Video](https://www.youtube.com/watch?v=Z7ydlOyjIWY) / [Slides](https://docs.google.com/presentation/d/1EfcsouyCFiM7gugesTmTcjAyBjLiceG2nnng1gUW0j4/edit?usp=sharing)|
| Workshop | CIS Digital Payments Master class 2018 || 11 Feb 2018 | |
| Talk | State of UPI | [NIPFP](https://nipfp.org.in/home-page/) Outreach | 15 Jun 2018 | [Slides](https://drive.google.com/open?id=1wBRX-LKxAsJC0ibhxh1xwN132yX4WpE7zlVPLSbrNDs)|
| Panel | Data protection and surveillance | [Nalsar Tech Law Forum](https://www.facebook.com/events/731709047163280/) | 09 Sep 2018 | [Video](https://www.youtube.com/watch?v=QraT867Lg6U)|
| Panel | Privacy, Data protection, Data localisation & Aadhaar | [Razorpay FTX](https://razorpay.com/ftx/) | 07 Dec 2018 | |
| Community Meetup | Discussion on RBIs Policy Paper on Authorisation of New Retail Payment Systems |[Hasgeek](https://hasgeek.com/50p/discussion-rbi-policy-paper-retail-payments-2019/) | 15 Feb 2019 | [Slides](https://docs.google.com/presentation/d/1Z0UUo3FEmzTWojogGXRW7SUUE_f1VXACTc0DWu6fvU4/edit?usp=sharing) |
| Workshop | Data and Consent in Financial Sector | [CUTS](https://www.cuts-ccier.org) Workshop | 19 Jul 2019 | [Slides](https://docs.google.com/presentation/d/1IAwq_6_RR94rY01CHmn5tU5_Vtz6Mrf0JCTbnPSdL10/edit)|
| Round Table | Privacy protection policy of India: Will the consumers feel empowered or enraged? | [CUTS](https://www.cuts-ccier.org) Workshop | 19 Jul 2019 | |
| Podcast | FASTag Conundrum | [Suno India - Cyber Democracy](https://www.sunoindia.in/cyber-democracy/) | 01 Dec 2019 | [Episode](https://www.sunoindia.in/cyber-democracy/fast-tag-conundrum/) |
| Podcast | FASTag in Tamil | [CashlessConsumer Podcast](https://castbox.fm/channel/CashlessConsumer---Digital-Payments-for-Consumers-id2486799) | 04 Dec 2019 | [Episode](https://castbox.fm/episode/FASTag---Tamil-Podcast-id2486799-id207398007)|
