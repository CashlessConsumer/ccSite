---
title: "Cashless Consumer Policy"
description: "All policy initiatives towards Cashless Consumer"
date: 2018-03-07T02:16:58-08:00
draft: false
categories: ['policy']
tags: ['poicy']
---

---

One of the cornerstones of Cashess Consumer has been to understand, engage, influence policy of digital payments for the consumer. Traditionally consumers are unrepresented block and government, industry, regulators often do not consider consumer perspectives, partly because they may be unaware of consumer viewpoint. Cashless Consumer will try to understand, engage, participate in all possible avenues to put forth consumer perspectives.

# Interests

* Consumer protection.
* Regulation, Competition and fair market practices.
* Cost of digital payments to consumers. (monetary, rights).
* Privacy, Data protection, cybersecurity.

# Work

* Our work on [Watal Report](https://cashlessconsumer.gitlab.io/watalreport)
* [Policy Tracker](https://www.cashlessconsumer.in/tags/policy/)
* Comments on [Information Technology (Security of Prepaid Payment Instruments) Rules 2017](https://docs.google.com/document/d/19Vtam4cmy9HQpoaFYLV5ZTgDkSPZzH7fPv9g9R-ytWw/edit)
* Participation in CUTS roundtable on [Digital Payments Innovation in Regulation to Manage Disruption](https://medium.com/cashlessconsumer/digital-payments-innovation-in-regulation-to-manage-disruption-d7e6d002b998)
* Participation in [Stakeholder consultation meeting by Fintech Steering Committee](https://drive.google.com/file/d/1qS8WypMVk3Fy62MzoIuQ6R4T9UMr6b-O/view?usp=drivesdk), DEA, FinMin.
* Complaint to RBI- BPSS about Auto Fetch of Bill data by BBPS entities - [Letter](https://docs.google.com/document/d/e/2PACX-1vSK2DtOHtQXhFmdgSv0EVqjg2dORT56FWCpSBnku-AjFzFxw9ayv-wfhaoROYG-eWiVzpXK0GF483Zh/pub) - [Blog](https://medium.com/cashlessconsumer/privacy-breach-in-bharat-bill-payment-system-2386c9f5296d) - [2020 Followup Letter](https://docs.google.com/document/d/e/2PACX-1vSDRNf1EyiLgEVJ0uugh2XSGrGgyAh1E9VrCcDd-E25XWMfyTk4yRQ6tYiwBJfXBSsx-uXxcfgnceOz/pub) - [Blog](https://medium.com/cashlessconsumer/following-up-on-consent-less-auto-fetch-privacy-violations-in-bbps-500ecc27fa30)
* Mandatory FASTag - Feedback on Motor  Vehicles (All India Tourist Vehicles Authorisation and Permit) Rules, 2018 [Letter](https://docs.google.com/document/d/e/2PACX-1vSQPlduk5v809FNylFZmWiAtI5gowYi4qZZV_OEeHhHJTQFnlLwgn6SMS8CuefTg7YGHUi919z3L-6D/pub) - [Blog](https://medium.com/cashlessconsumer/feedback-on-mandatory-fastag-323e7b4f9359)
* Response to RBI Consultation on Authorisation of Retail Payment Systems, Feb 2019 - [Response](https://docs.google.com/document/d/e/2PACX-1vQ890MrmxeXC9UCfJgVndSEXYdeBB4iCnM7rGLxExMUhXOH6dc5EP3nUvoApqleoJyFFARTbEUpXv9J/pub) - [Blog](https://medium.com/cashlessconsumer/response-to-rbi-consultation-on-authorization-of-new-retail-payment-systems-8099f39558ab)
* Response to RBI Discussion paper on Guidelines for Payment Gateways and Payment Aggregators - [Response](https://docs.google.com/document/u/1/d/e/2PACX-1vQlQhxfb2oxuhbvDVQN3OBZAT-L7LoYdB4U455xT99cVpS7nTahh2OY9LIl-z0lq4lEejAC4K6MDFIL/pub)
* Response to Draft Framework for authorisation of a pan-India New Umbrella Entity (NUE) for Retail Payment Systems -  [Response](https://docs.google.com/document/u/1/d/e/2PACX-1vSYbrgKj-pQeKAMk1RZ_3ewjTbbXo75VS9K85kzqaRQN6V_OeDOfen1iDZ0S0-eGXJ0XscRfkODERBM/pub)
* Response to Draft framework for recognition of a Self-Regulatory Organisation for Payment System Operators - [Response](https://docs.google.com/document/d/1206yajCWntghQkoY1qn7Me4WNVqJY8Q6xV0cclsRvuY/edit?usp=sharing)
