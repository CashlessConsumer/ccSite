---
title: "About"
description: "About CashlessConsumer"
date: 2018-03-07T02:16:58-08:00
draft: false
layout: subsection
slug: about
---



CashlessConsumer is a consumer collective working on digital payments to increase awareness, understand technology, produce /consume data, represent consumers in policy of digital payments ecosystem to voice consumer perspectives, concerns with a goal of moving towards a fair cashless society. Read more about the [founding principles of the collective.(↪)](https://medium.com/cashlessconsumer/cashlessconsumer-why-a689bec858d1)


* [Disclaimers](/about/disclaimers)
* [People](/about/people)
* [Contact](/about/contact)
