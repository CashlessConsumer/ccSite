---
title: "Draft Framework for authorisation of a pan-India New Umbrella Entity (NUE) for Retail Payment Systems"
description: "Draft Framework for authorisation of a pan-India New Umbrella Entity (NUE) for Retail Payment Systems"
date: 2020-02-26T09:04:20+05:30
categories: ["awareness", "policy"]
tags: ["policy","discussion","policytracker"]
draft: false
---

# Introduction

On Feb 10,2020 Reserve Bank of India has tabled a draft Framework for authorisation of a pan-India New Umbrella Entity (NUE) for Retail Payment Systems.

* [Announcement](https://www.rbi.org.in/Scripts/BS_PressReleaseDisplay.aspx?prid=49373)
* [Draft Framework](https://www.rbi.org.in/scripts/bs_viewcontent.aspx?Id=3832)

# Media Articles
* [RBI proposes draft rules to set up alternative retail payments system](https://www.livemint.com/industry/banking/rbi-proposes-draft-rules-to-set-up-alternative-retail-payments-system-11581407149088.html), February 11, Livemint
* [RBI Sets The Stage For A Competitor To NPCI. But Why?](https://www.bloombergquint.com/business/rbi-sets-the-stage-for-a-competitor-to-e-pay-platform-npci-but-why), February 13, BloombergQuint
* [RBI is trying to change payments in India, again](https://themorningcontext.com/rbi-changing-payments-in-india-again/), February 13, 2019, The Morning Context


# Policy Perspetives / Submissions

* [The Hindu Businessline Editorial](https://www.thehindubusinessline.com/opinion/editorial/rbi-move-to-create-a-counterpart-to-npci-needs-a-rethink/article30915180.ece)
* [Dvara Research Submission](https://www.dvara.com/research/wp-content/uploads/2020/03/Dvara-Research-Response-to-RBI-NUE-Framework.pdf) - [Archive](https://web.archive.org/web/20200312101431/https://www.dvara.com/research/wp-content/uploads/2020/03/Dvara-Research-Response-to-RBI-NUE-Framework.pdf) - [Blog](https://www.dvara.com/blog/2020/03/12/our-response-to-the-draft-framework-for-authorisation-of-a-pan-india-new-umbrella-entity-nue-for-retail-payment-systems/)
* [NASSCOM Submission](https://community.nasscom.in/communities/policy-advocacy/representation-to-rbi-on-draft-framework-for-authorisation-of-a-pan-india-new-umbrella-entity-nue-for-retail-payment-systems.html)
* [Cashless Consumer Submission](https://docs.google.com/document/u/1/d/e/2PACX-1vSYbrgKj-pQeKAMk1RZ_3ewjTbbXo75VS9K85kzqaRQN6V_OeDOfen1iDZ0S0-eGXJ0XscRfkODERBM/pub)
