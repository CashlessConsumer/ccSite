---
title: "Opinions"
description: "List of media articles, opinion pieces"
date: 2018-10-28T09:04:20+05:30
categories: ["awareness", "policy"]
tags: ["media","explainer","opinon","analysis"]
draft: false
---

 Article | Author(s) |  Date | Publisher  | Notes |
|---|---|---|---|---|
|[FASTag: Will Datafication of India’s Tolls Boost Highway Development?](https://thewire.in/political-economy/fastag-will-datafication-of-indias-tolls-boost-highway-development) | Srikanth L | December 14, 2019 | The Wire | FASTag Explainer |
|[Exclusive: RBI issues in-principle licenses to 5 Account Aggregators](https://www.medianama.com/2018/11/223-exclusive-rbi-issues-in-principle-licenses-to-5-account-aggregators/) | Srikanth L | November 19, 2018 | Medianama | Account Aggregator licenses and FInancial data flows |
|[Unpacking RBI's Quest to Have All Payment Data Stored Within India's National Boundaries](https://thewire.in/business/rbi-payment-data-localisation-india) |  Jyoti Panday and Srikanth | October 27, 2018 | The Wire | On data localisation, payments regulation |
|[As UPI 2.0 Is Unveiled, It Remains Very Much a Transaction in Progress](https://thewire.in/tech/upi-2-0-is-unveiled-transaction-in-progress) | Srikanth | Aug 21, 2018 | The Wire | UPI 2.0 |
|[Is Cashback an efficient tool for growth? and role of state in promotion of digital payments](https://www.medianama.com/2018/07/223-bhim-mobikwik-response/) | Srikanth | July 2, 2018 | Medianama | Incentivisation of payments by state |
|[Going cashless after demonetisation? Compare eWallets and UPI apps for what suits you best](https://scroll.in/article/823024/going-cashless-after-demonetisation-compare-ewallets-and-upi-apps-for-what-suits-you-best) | Srikanth | Dec 3, 2016 | Scroll | Explainer on wallets, UPI |
| [UPI is a toll road](https://www.medianama.com/2016/10/223-upi-is-a-toll-road/) | Srikanth | Oct 18, 2016 | Medianama | UPI being public good|
