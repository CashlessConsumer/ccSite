---
title: "Technology Specifications"
description: "Technology Specification of Payment Systems / Infrastructure"
date: 2019-11-23T09:04:20+05:30
categories: ["awareness","tech"]
tags: ["Tech","Design","Payment Systems"]
draft: false
---

# Technology Specifications

As part of monitoring the payments ecosystem, we study technology systems that power payments landscape. Listed here is publicly available documentation related to payment systems / supporting infrastructure. If you find something missing that is a good addition, please let us know through [contact](about/contact) and we will be glad to add to this library.

| S.No | Payment System | Specification | Archive | Comments |
|---|---|---|---|---|
|1| Unified Payments Interface | [Spec 1.0](https://www.mygov.in/digidhan/pages/pdf/sbi/NPCI%20Unified%20Payment%20Interface.pdf) | [Archive](https://archive.vn/xZEW0) | V1.0 released in 2015. v2.0 in 2018 has not been made public |
|2| UPI - Deep linking Specification | [Spec 1.6](https://www.npci.org.in/sites/default/files/UPI%20Linking%20Specs_ver%201.6.pdf) | [Archive](https://archive.ph/2jtkw) | V1.6 released in Nov 2017, v2.0 with signature support has not been made pubic |
|3| Bharat Bill Payment System | [Spec 13.0](https://www.npci.org.in/sites/default/files/BBPS%20API%20Specifications%20v13.0%20-%2005112019.pdf) | [Archive](https://archive.vn/CLK8u) | Effective V2 of BBPS, in 2019 |
|4| National Electronic Toll Collection | [Spec 1.16](https://www.npci.org.in/sites/all/themes/npcl/images/PDF/ETC_API_Technology_Specification_V1_16.pdf) | [Archive](https://archive.vn/rEJ1p) | NETC API Specification - 2018 |
|5| SFMS Messaging Standards | - | [Archive](https://web.archive.org/web/20060917052537/http://www.idrbt.ac.in/infinet/infinet.html) | SFMS is messaging standard that Indian domestic financial messaging uses.|
|6| RTGS Messaging Standards | [NG-RTGS Specifications](https://rbidocs.rbi.org.in/rdocs/RTGS/ZIPs/XSFS06032013FL.zip) |[Archive](https://web.archive.org/web/20130525210500/https://rbidocs.rbi.org.in/rdocs/RTGS/ZIPs/XSFS06032013FL.zip) | Released in 2013 [Press Release](https://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=28270) | 
|7| Cheque Truncation System Clearing house Interface| [Spec 2.5](https://www.npci.org.in/sites/all/themes/npcl/images/PDF/CHI_SpecificationdocumentVersion2.5.pdf) |  | As of July 2016
|8| Micro ATM Standards | [Ver 1.5.1](https://www.npci.org.in/sites/all/themes/npcl/images/PDF/MicroATM_Standards_v1.5.1_Clean.pdf) | [Archive](https://archive.ph/tZGWh) | As of 2013.
|9| AePS Interface Specification | Ver 3.0.0 | [Archive](https://web.archive.org/web/https://www.npci.org.in/documents/AEPS%20Interface%20Specification%20v3.0.pdf) | As of June 2012.
|10| IMPS - Interoperability Standards for Mobile Payments | V 1.10 | [Archive](https://web.archive.org/web/20160617021933/http://mpf.org.in/pdf/Interop_Std_V1_10.pdf) | TeneT Group, September 2009.
|11| IMPS - Interoperability Standards for Mobile Payments | V1. A1 | [Archive](https://web.archive.org/web/20170208192835/http://mpf.org.in/pdf/MPFI_Interop_Std_V1.A.pdf) | TeneT Group, November 2010.
|12| Interface Specification of NCMC Ecosystem | Version 1.2 (Part IV to Part VII) | [Archive](https://web.archive.org/web/https://stqc.gov.in/sites/default/files/NCMCInterfaceSpecifications(PartIV-VII).pdf) | CDAC, Feb 2020 |
|13| ONMAGS - Online Mandate Approval Gateway System |  V 4.2 | [Archive](https://web.archive.org/web/0/https://www.npci.org.in/PDF/nach/circular/2022-23/Circular-no.009-E-Mandate-throught-Aadhaar-based-authentication.pdf) | NPCI, May 2022.


# Global Standards

* [ISO 15022](https://www.iso20022.org/15022/iso-15022-home) - MT Standard
* [ISO 20022](https://www.iso20022.org/) - MX Standard
    
