---
title: "Card"
description: ""
date: 2018-06-27T09:04:20+05:30
categories: []
tags: ["Card"]
draft: false
---

# Salient Features / Notes

* Cashless Consumer is a 16 character phrase that lets people know what consumer rights are in digital payments
* The card represent card, chip, qr code modes of payment and the background represents a fair balance of rights between consumers and institutions
* 811 signifies Nov 8, [demonetisation day](https://en.wikipedia.org/wiki/2016_Indian_banknote_demonetisation)
* The CC on card signifies both CashlessConsumer and that its licensed under Creative Commons.

* Get your Cashless Consumer card now, for free

![CC Card Front](uploads/CashlessConsumerFrontside.jpg)
![CC Card Back](uploads/CashlessConsumerBackside.jpg)

Card design by Veeven. V
<!--more-->
