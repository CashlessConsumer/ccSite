---
title: "Committee on Deepening Digital Payments"
description: "Tracking Deepening Digital Payments Committee"
date: 2019-02-28T09:04:20+05:30
categories: ["awareness", "policy"]
tags: ["policy","committee","policytracker"]
draft: false
---

# Introduction

On Jan 8,2019 Reserve Bank of India has decided to constitute a High-Level Committee on Deepening of Digital Payments.

* [Announcement](https://rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45949)
* Twitter Hashtag - [#DeepeningDigitalPayments](https://twitter.com/hashtag/DeepeningDigitalPayments)

# Media Articles
* [PCI Recommends Independent Payment Expert, Part-KYC To Nilekani Committee](https://inc42.com/buzz/pci-recommends-independent-payment-expert-part-kyc-to-nilekani-committee/), February 9, 2019, Inc42
* [Inadequate ATM infra could hit India's financial inclusion plan](https://tech.economictimes.indiatimes.com/news/corporate/inadequate-atm-infra-could-hit-indias-financial-inclusion-plan/68260279) CATMi submission - March 5, 2019,
* [Business correspondents ask for self-regulatory body to standardise industry](https://tech.economictimes.indiatimes.com/news/corporate/business-correspondents-ask-for-self-regulatory-body-to-standardise-industry/68321301) - March 8, 2019, Economic Times.
* [Do away with KYC for low-value digital transactions, Nasscom tells RBI Panel](https://www.thehindubusinessline.com/money-and-banking/do-away-with-kyc-for-low-value-digital-transactions-nasscom-tells-rbi-panel/article26561316.ece) - March 17, 2019, The Hindu Businessline. 
* [Regulating payments: One too many voices](https://economictimes.indiatimes.com/markets/stocks/news/his-masters-voice-one-too-many/articleshow/68491036.cms) - March 20, 2019, Economic Times.
* [RBI panel for making all public payments digital](https://www.livemint.com/news/india/rbi-panel-for-making-all-public-payments-digital-1556132461682.html) - April 25, 2019, Livemint

# Policy Perspetives / Submissions

* [Users' Perspectives on Digital Payments](http://www.cuts-ccier.org/Payments-Infrastructure/pdf/Presentation_for_RBI_Committee_on_Deepening_Digital_Payments.pdf), Feb 2019, CUTS International
* [To surcharge or not to surcharge! The plight of small and medium merchants](http://www.math.iitb.ac.in/~ashish/workshop/surcharge-2019_03_03.pdf), March 2019, Ashish Das - Department of Mathematics, Indian Institute of Technology Bombay.
* [Introduce incentives to widen digital payments in India](https://www.moneycontrol.com/news/business/personal-finance/introduce-incentives-to-widen-digital-payments-in-india-3626401.html), March 11, 2019, TR Ramachandran, Group Country Manager, India & South Asia for Visa. 
* [Business Correspondants Federation of India Submission](http://bcfi.org.in/wp-content/uploads/2019/03/BCFI-Memorandum-to-CDDP-Post-Presentation-190319.pdf), 19 March 2019, BCFI
