---
title: "Killer Loan Apps"
description: "Cashless Consumer on Killer Loan Apps"
date: 2021-02-11T02:04:20+05:30
categories: ["awareness", "policy", "data"]
tags: ["killerloanapps","digital lending","analysis", "osint"]
draft: false
---

# KillerLoanApps

CashlessConsumer has been involved in studying the operations of fly-by-night rogue digital lenders as well as broader digital lending ecosystem, following constant complaints of harassment by app based digital lenders which has led to even suicides. This page is a collection of all our efforts related to #KillerLoanApps. We would like to thank everyone involved in fighting the killer loan apps, especially reporters, startup founders, law enforcement, victims of these apps - all of whom provided crucial information in solving the complex puzzle. 

## Awareness 
* Following helped us in understanding the context of the problem in detailed manner.
### Research
* [Opera: Phantom of the Turnaround – 70% Downside](https://hindenburgresearch.com/opera-phantom-of-the-turnaround/) - Jan 2020
* [Operation Hafta Vasooli](https://www.zeebiz.com/india/news-operationhaftavasooli-zee-business-sting-operation-busts-mafia-recovery-agents-small-lending-companies-124216) - Zee Business
* [50+ News reports](https://docs.google.com/spreadsheets/d/e/2PACX-1vSmxUZHAYBAqfciWvG2Ksj4rNwINpSP-9rNpI7Pxxh2AtsopGlZPSkEbx5Iool7X0nwvIE4CFBQgpIM/pubhtml?gid=1406325730&single=true) from newspapers in Nov, Dec 2020.

## Technology
* Reversing selected apps and studying them in detail using #OSINT tools such as [Koodous](koodous.com), [MobSF](https://mobsf.github.io/), [Certificate Search](https://crt.sh/). We then grokked Koodous with variety of search filters to get long list of apps and began extracting metadata from them to plot the app network.
* Search playstore for popularly used search terms and enhance app list. Extract metadata, and create enable monitoring Google Play and create *LoanApp Database* and iterate the process over a month - [Code](https://gitlab.com/-/snippets/2053946) uses [Google Play Scraper API](https://github.com/facundoolano/google-play-api)
* [Banbreach](http://banbreach.com/) is building a shell company network mapper by extracting, visualizing [Registrar of Companies](https://mca21.gov.in/) data and the analysis using the alpha build of the mapper gave crucial leads in mapping the network of operators behind the apps.
* Archive, Mirror and study meta app generators - [CashlessConsumer Mirror](https://gitlab.com/CashlessConsumer/research/DigitalLending/pytxxy)
* Collaborate with startups serving the loan apps ecosystem for intelligence sharing.

## Data
* [LoanApp Database](https://dbhub.io/srikanthlogic/loanapps.db) - Database of 1000+ loan apps along with their playstore metadata, permissions sourced between Dec 25, 2020 - Jan 15, 2021
* [Tracking Google Play Deletions](https://docs.google.com/spreadsheets/d/e/2PACX-1vSmxUZHAYBAqfciWvG2Ksj4rNwINpSP-9rNpI7Pxxh2AtsopGlZPSkEbx5Iool7X0nwvIE4CFBQgpIM/pubhtml?gid=0&single=true)
* [Suicide Tracker](https://twitter.com/logic/status/1331894789483053058)

## Policy
* Our member [Anivar Aravind](https://anivar.net), volunteer [Swathanthra Malayalam Computing](https://smc.org.in/), wrote to Finance Minister of Kerala, on regulating digital lending and an annoucement of proposed amendment to [Kerala Money Lenders Act, 1958](https://keralataxes.gov.in/wp-content/uploads/2017/08/KML_ACT.pdf) was [made](https://twitter.com/anivar/status/1349989877174075394) in Kerala Budget 2021.
* CashlessConsumer has been in touch with [@CyberDost](http://twitter.com/CyberDost), a Cyber-safety and Cybersecurity awareness initiative by Ministry of Home Affairs, Government of India and sharing our knowledge, suggestions on combatting the menace of KillerLoanApps
* CashlessConsumer will also be writing to RBI Working Group on Digital Lending shortly with a detailed proposal based on our work.

## Advocacy
* Following lists our interactions with journalists about the problem.

### Media Stories
* [Google removes 5 rogue lending applications](https://timesofindia.indiatimes.com/india/Google-removes-5-rogue-lending-applications/articleshow/79253637.cms) - Anam Ajmal, Times of India, Nov 17, 2020
* [Instant Money Apps: Loan sharks are now on your mobile](https://timesofindia.indiatimes.com/india/loan-sharks-are-now-on-your-mobile/articleshow/79542963.cms) - Srikkanth D, Times of India, Dec 3, 2020
* [Chinese loan apps mushrooming in India, threaten borrowers with social shaming, harassment, FIRs](https://www.nationalheraldindia.com/india/chinese-loan-apps-mushrooming-in-india-threaten-borrowers-with-social-shaming-harassment-firs) - Ashlin Mathew, National Herald, Dec 9, 2020
* [Regulation needed to monitor money-lending applications: Experts](https://www.newindianexpress.com/lifestyle/tech/2020/dec/23/regulation-needed-to-monitor-money-lending-applications-experts-2239898.html) - Sahaya Novinston Lobo, Dec 23, 2020
* [India’s instant loan app crisis is made in China](https://the-ken.com/story/indias-instant-loan-app-crisis-is-made-in-china/) - Arundhati Ramanathan, The Ken, Jan 11, 2021
* [Made in China: How the instant loan app racket boomed in India](https://www.thenewsminute.com/article/made-china-how-instant-loan-app-racket-boomed-india-141331) - Mithun MK, The News Minute - Jan 12, 2021
* [Google cracks down on personal loan apps in India following abuse and outcry](https://techcrunch.com/2021/01/13/google-cracks-down-on-personal-loan-apps-in-india-following-abuse-and-outcry/) - Manish Singh, TechCrunch, Jan 14, 2021
* [Google Takes Down Around 200 Lending Apps In India Clean-Up: BQ Exclusive](https://www.bloombergquint.com/bq-blue-exclusive/google-takes-down-nearly-200-lending-apps-in-india-clean-up-bq-exclusive) - Vishwanath Nair, Jan 15, 2021    
* [Google removes 30 loan apps from Play store after RBI red flag](https://economictimes.indiatimes.com/tech/technology/google-removes-personal-loan-apps-violating-user-safety-policies-from-play-store/articleshow/80267043.cms) - Economic Times, Jan 15, 2021
* [How instant loan apps became a death trap](https://timesofindia.indiatimes.com/business/india-business/how-instant-loan-apps-became-a-death-trap/articleshow/80263999.cms) - Times of India, Jan 15, 2021 
* [Troves Of Data Stolen By Fake Digital Lending Apps](https://www.medianama.com/2021/01/223-data-stolen-by-fake-digital-lending-apps/) - Advait Palepu, Medianama, Jan 18, 2021
* [Shame, suicide and the dodgy loan apps plaguing Google’s Play Store](https://www.wired.co.uk/article/google-loan-apps-india-deaths) - Varsha Bansal, Wired, Jan 20, 2021
* [Debt and shame via Google Play](https://restofworld.org/2021/debt-and-shame-via-google-play/) - Nilesh Christopher, RestofWorld, Jan 27, 2021
* [How Loan Apps Weaponise Your Data to Make You Pay](https://www.vice.com/en/article/4ad5bw/how-loan-apps-weaponise-your-data-to-make-you-pay) - Pallavi Pundir, Vice, Feb 1, 2021
* [The unregulated tech supermarket powering India’s loan app scourge](https://the-ken.com/story/the-unregulated-tech-supermarket-powering-loan-app/) - Arundhati Ramanathan, The Ken, Mar 17, 2021 - [CashlessConsumer Research](https://freeradical.zone/@cashlessconsumer/105903517333221182)

### Video
* [#KillerLoanApps, Detecting Predatory fintech apps](https://www.youtube.com/watch?v=OV7PCvfsF-A) [Slides](https://1drv.ms/p/s!Au4kkP8CklsegbU_6MZHhyTbb_uy7w?e=jVzvtl) - [CashlessConsumer Payment Deepdives](https://hasgeek.com/cashlessconsumer/killerloanapps-detecting-fake-fintech-apps/) - Jan 10, 2021
* [Loan terror in Digital India](https://www.ndtv.com/video/shows/ndtv-special-ndtv-24x7/online-loan-app-racket-loan-terror-in-digital-india-571961) - NDTV Investigation - Jan 11, 2021

### Podcast
* [Harassment by digital lending apps a matter of concern](https://www.sunoindia.in/the-suno-india-show/digital-lending-apps-and-platforms-become-a-matter-of-concern/) - [Suno India Show](https://www.sunoindia.in/the-suno-india-show) with [Srinivas Kodali](https://twitter.com/digitaldutta) - Dec 28, 2020
* [Digital Loan Sharks](https://www.aaw.az/et/digital-loan-sharks/) - [The Morning Brief, Economic Times Awaaz](https://www.aaw.az/et) - Jan 19, 2021
* [கடன் வழங்கும் செயலிகளிடம் சிக்குவது யார்? தப்ப வழி உண்டா? சட்டம் என்ன செய்கிறது?](https://radiopublic.com/thozhar-podcast-GM9l1e/s1!48e54) - [Thozhar Podcast](https://radiopublic.com/thozhar-podcast-GM9l1e) with [Sindhan](https://twitter.com/sindhan) - Jan 19, 2021

### Blog
* [(Fake) Digital Lending apps & Collection Harassment](https://medium.com/cashlessconsumer/fake-digital-lending-apps-collection-harassment-8e56ab5ac0bd) - CashlessConsumer Blog, Nov 10, 2020
* [(Fake) Digital Lending apps Database](https://medium.com/cashlessconsumer/fake-digital-lending-apps-database-b5de323f6d1b) - CashlessConsumer Blog, Dec 28, 2020
* [Tweets from:logic on #KillerLoanApps](https://twitter.com/search?q=from%3Alogic%20%23KillerLoanApps&src=typed_query&f=live)
