---
title: "Stakeholder Mapping for Fintech / Payments Policy in India"
description: "Stakeholder Mapping for Fintech / Payments Policy in India"
date: 2018-10-28T09:04:20+05:30
categories: ["awareness", "policy"]
tags: ["policy","stakeholders","people"]
draft: false
---

# Introduction

Incomplete list of organizations, entities interested in payments policy

# Industry Bodies

* [Payments Council of India](http://paymentscouncil.in/) 
* [Fintech Convergance Council](https://twitter.com/FCCUpdates)
* [Confederation of ATM Industry - CATMi](https://catmi.org/)
* [Business Correspondants Federation of India](http://bcfi.org.in/)


# ThinkTanks
* [iSpirt](https://ispirt.in/) - Indian Software Products Round Table
* [Dvara Research](https://www.dvara.com/research/) - Financial Services Policy Advocacy
* [CGAP](https://www.cgap.org) - Consultative Group to Assist the Poor, World Bank initiative on financial inclusion.
* [CUTS](http://www.cuts-international.org) - Consumer Unity & Trust Society

# Other Groups