---
title: "RTI Desk"
description: ""
date: 2018-10-23T09:04:20+05:30
categories: ["data", "rti"]
tags: ["rti"]
draft: false
---

# Right To Information Desk

![Cashless Consumer RTI Desk](uploads/CC-RTIDesk.png)

[Right to Information Act](https://en.wikipedia.org/wiki/Right_to_Information_Act%2C_2005) is a transparency legislation in India. At Cashless Consumer, we intend to positively use RTI in bringing out any / all information that is possible to be obtained through use of RTI against organizations having RTI under its mandate. We will seek information related digital payments, payments regulation, payments statistics, digital payments promotion policies, consumer protection, regulatory actions, transparency in regulation making among others. Although we will mostly focus on PayTech, some aspects related to banking, larger fintech also interest us, as they closely impact payment technology & its use cases.

* [List of RTI](post/rtilist)
* [File RTI using RTI Desk](post/filerti)
* [All RTI Replies](tags/rtireply)
