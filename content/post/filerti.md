---
title: "File your RTI"
description: "File RTI - CashlessConsumer"
date: 2018-10-23T09:04:20+05:30
categories: ["data", "rti"]
tags: ["rti"]
draft: false
---

# Filing RTI

RTI requets can be filed by Indian Citizens through [online portal](https://rtionline.gov.in/). Register and login to file RTI requests to 2200 central government departments. Since payments / banking is a central subject, in most cases this should be sufficient. However when the required information lies with state government department, please find means to raise RTI. If you need help, please feel free to [contact](about/contact).

We encourage you to file RTIs on all things payments and share [responses with us](about/contact) to make it available for wider audience. In case if you need to file RTIs anonymously for anything that is related to payments, you can drop a request and volunteers at CashlessConsumer will consider requesting for the same after deeming fit. [Request here for filing RTI](https://docs.google.com/forms/d/e/1FAIpQLSeaQrTmj4b6BpGCp_654GPk34-LL0LRtKZW4E6Ts9QhtHjqoQ/viewform?usp=sf_link)

* [RTI Desk](post/rti)
* [List of RTI](post/rtilist)
* [All RTI Replies](tags/rti)