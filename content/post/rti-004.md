---
title: "RTI Desk - RTI-004"
description: "RTI 004 - NBFC Account Aggregator licenses"
date: 2018-11-06T17:04:20+05:30
categories: ["rtireply"]
tags: ["RBI", "Awareness", "Campaign"]
draft: false
---

# RTI 004 - NBFC Account Aggregator licenses

------------------------
| Field | Value |
|---|---|
| RTI ID | RBIND/R/2018/55014|
| Date of Filing | 12/10/2018 |
| PIO | Reserve Bank of India |
| Subject | NBFC Account Aggregator licenses |
| Query | 1. Please provide number of applicants to NBFC-Account Aggregator class of entities requesting NBFC-AA license to undertake account aggregation.<br><br> 2. Please provide list of entities that RBI has issued in-principle or final approval for operating as NBFC-Account Aggregators along with date of approval. |
| Response | 1. As on date, the bank has received 9 applications for registration as NBFC-AA <br><br> 2. Till date, the bank has granted in-principle approval to 5 applicants as NBFC-AA. List of entities which will be accorded final approval/Certificate of Registration for commencing operations will be placed on our website.|
| Date of Response | 06/11/2018 |
| Twitter | [Tweet](https://twitter.com/logic/status/1059746679945981952) |
| Notes | #RBI, #Awareness, #License, #ConsentLayer, #DataSharing  |

* [RTI Desk](post/rti)
* [File RTI using RTI Desk](post/filerti)
* [List of RTI](post/rtilist)
* [All RTI Replies](tags/rtireply)