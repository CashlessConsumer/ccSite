---
title: "Discussion paper on Guidelines for Payment Gateways and Payment Aggregators"
description: "Discussion paper on Guidelines for Payment Gateways and Payment Aggregators"
date: 2019-10-22T09:04:20+05:30
categories: ["awareness", "policy"]
tags: ["policy","discussion","policytracker"]
draft: false
---

# Introduction

On Sep 17,2019 Reserve Bank of India has tabled a discussion paper on guidelines for payment gateways and payment aggregators.

* [Announcement](https://www.rbi.org.in/Scripts/BS_PressReleaseDisplay.aspx?prid=48173)
* [Discussion Paper](https://www.rbi.org.in/Scripts/PublicationReportDetails.aspx?UrlPage=&ID=943)

# Media Articles
* [RBI proposes regulation, licensing of payment aggregators and gateways](https://www.medianama.com/2019/09/223-rbi-payment-aggregators-and-gateways/), September 18, Medianama
* [NASSCOM Community Policy brief](https://community.nasscom.in/communities/policy-advocacy/policy-brief-guidelines-for-payment-gateways-and-payment-aggregators.html), September 21, NASSCOM Community
* [Razorpay aims to work with RBI on payment gateways’ norms](https://www.livemint.com/companies/news/razorpay-looking-to-work-closely-with-rbi-on-new-regulations-for-payment-gateways-hires-two-senior-leaders-11570953725906.html), October 13, 2019, Livemint


# Policy Perspetives / Submissions

* [CUTS International Submission](https://drive.google.com/file/d/15sjAjo475dP8KRtTLdQI1DiDuXAu6Asg/view)
* [Dvara Research Submission](https://www.dvara.com/research/wp-content/uploads/2019/10/Comments-to-the-Reserve-Bank-of-India-on-the-Discussion-Paper-on-Guidelines-for-Payment-Gateways-and-Payment-Aggregators.pdf)
* [NASSCOM Submission](https://community.nasscom.in/wp-content/uploads/attachment/19420-20191104-rbi-papg-nasscom-submission.pdf)
* [Vidhi Legal Submission](https://www.medianama.com/wp-content/uploads/Vidhi-comments-RBI-PA-PG.pdf)
* [Cashless Consumer Submission](https://docs.google.com/document/u/1/d/e/2PACX-1vQlQhxfb2oxuhbvDVQN3OBZAT-L7LoYdB4U455xT99cVpS7nTahh2OY9LIl-z0lq4lEejAC4K6MDFIL/pub)
